-- Execute it on the Principal server instance,set the mirror server instance as the partner.

DECLARE @SQL NVARCHAR(3000)
DECLARE @DBNAME NVARCHAR(300)
DECLARE @Domain NVARCHAR(300)
DECLARE @FQDN NVARCHAR(300)
DECLARE @MirrorHost NVARCHAR(300)


SET @MirrorHost= 'AD1SHUPCSQL19'  -- Replace the Mirror server host name according to your environment.

EXEC master.dbo.xp_regread 'HKEY_LOCAL_MACHINE', 'SYSTEM\CurrentControlSet\services\Tcpip\Parameters', N'Domain',@Domain OUTPUT

Print @Domain

SET @FQDN = @MirrorHost+'.'+@Domain;

/*As below SELECT statement executed on principal server, hence it should updated with principal sql instance name */

SET @DBName= (select top 1 DBName from  [master].dbo.AddDBMirroring ORDER BY CREATEDate desc)

SET @SQL = 'ALTER DATABASE '+ @DBName+ ' SET PARTNER ='+'''TCP://'+@FQDN+':7024'''

print @SQL

Exec (@SQL)

GO  
