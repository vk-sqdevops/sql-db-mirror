USE master
go

IF NOT EXISTS (SELECT * FROM sys.certificates WHERE name like 'HOST_A_cert')
BEGIN

CREATE CERTIFICATE HOST_A_cert WITH SUBJECT = 'HOST_A certificate for database mirroring'

BACKUP CERTIFICATE HOST_A_cert TO FILE = '\\AD1SHUPCSQL22\BACKUPS\HOST_A_cert.cer';  

END
ELSE

BEGIN
 PRINT 'Certificate already exists'
END 
