USE master
go

IF NOT EXISTS (SELECT * FROM sys.certificates WHERE name like 'HOST_B_cert')
BEGIN

CREATE CERTIFICATE HOST_B_cert WITH SUBJECT = 'HOST_A certificate for database mirroring'

BACKUP CERTIFICATE HOST_B_cert TO FILE = '\\AD1SHUPCSQL22\BACKUPS\HOST_B_cert.cer';  

END
ELSE

BEGIN
 PRINT 'Certificate already exists'
END 
