USE master;  
GO  


IF NOT EXISTS (SELECT state_desc,type_desc FROM sys.database_mirroring_endpoints WHERE name='Mirroring')
BEGIN

CREATE ENDPOINT Endpoint_Mirroring  
   STATE = STARTED  
   AS TCP (  
      LISTENER_PORT=7024  
      , LISTENER_IP = ALL  
   )   
   FOR DATABASE_MIRRORING (   
      AUTHENTICATION = CERTIFICATE HOST_B_cert  
      , ENCRYPTION = REQUIRED ALGORITHM AES  
      , ROLE = ALL  
   );  


END

DECLARE @state_desc nvarchar(100)

SET @state_desc=(SELECT state_desc FROM sys.database_mirroring_endpoints WHERE name='Mirroring')

IF @state_desc<>'STARTED'
BEGIN

	ALTER ENDPOINT Mirroring STATE=STOPPED

	PRINT 'Starting endpoint'

	ALTER ENDPOINT Mirroring STATE=STARTED


END

