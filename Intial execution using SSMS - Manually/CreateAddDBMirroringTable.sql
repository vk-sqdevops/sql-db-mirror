USE [master]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AddDBMirroring](
	[DBName] [nvarchar](100) NULL,
	[CreateDate] [datetime] NULL,
	[BackupFileName] [nvarchar](1000) NULL,
	[BackupPath] [nvarchar](1000) NULL
) ON [PRIMARY]

GO


