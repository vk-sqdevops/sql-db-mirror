USE [master]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE  TRIGGER [ddl_trig_database]   
ON ALL SERVER   
FOR CREATE_DATABASE   
AS 
DECLARE @DBNAME NVARCHAR(300)

    PRINT 'Database Created.'  
	SET @DBNAME= (select CAST(eventdata().query('/EVENT_INSTANCE/DatabaseName[1]/text()') as NVarchar(128)))

	DECLARE @@MESSAGE NVARCHAR(1000)
	SELECT @@MESSAGE = 'A new database: ' + @DBNAME + ' has been created at '+CONVERT(VARCHAR(30),GETDATE(),113) +''
  

	EXEC master..xp_logevent 50002, @@MESSAGE, ERROR;

	INSERT INTO [master].dbo.AddDBMirroring VALUES(@DBNAME,GETDATE(),NULL,NULL)
	

GO

ENABLE TRIGGER [ddl_trig_database] ON ALL SERVER
GO


