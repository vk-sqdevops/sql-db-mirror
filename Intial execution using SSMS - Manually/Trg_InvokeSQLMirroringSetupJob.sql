USE [master]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[Trg_InvokeSQLMirroringSetupJob] ON [dbo].[AddDBMirroring]

AFTER INSERT
AS
BEGIN

 EXEC msdb..sp_start_job @job_name = 'Database_Mirroring_Automation'

END 
GO


