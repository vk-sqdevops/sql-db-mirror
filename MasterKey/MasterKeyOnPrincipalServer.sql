USE master
go

IF NOT EXISTS (SELECT * FROM sys.symmetric_keys WHERE symmetric_key_id = 101)
BEGIN

  PRINT 'Creating Database Master Key'
  CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'MyPass#1'

END
ELSE

BEGIN
 PRINT 'Database Master Key already exists'
END 