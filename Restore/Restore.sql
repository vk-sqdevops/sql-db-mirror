USE [master]

DECLARE @DBName NVARCHAR(300)
DECLARE @BackupFileName NVARCHAR(300)
DECLARE @BackupPath NVARCHAR(500)
DECLARE @SQL NVARCHAR(2000)


SET @DBName= (select top 1 DBName from  [AD1SHUPCSQL22].[master].dbo.AddDBMirroring ORDER BY CREATEDate desc)
SET @BackupFileName= (select top 1 BackupFileName from  [AD1SHUPCSQL22].[master].dbo.AddDBMirroring ORDER BY CREATEDate desc)


SET @BackupPath='\\AD1SHUPCSQL22\Backups\' /* Replace the principal Host name and make sure folder Backups is shared. */

SET @SQL= 'RESTORE DATABASE '+ @DBName +' FROM  DISK = N'''+ ''+@BackupPath+@BackupFileName+''' WITH  FILE = 1,NORECOVERY,  REPLACE'

Exec (@SQL)

SET @SQL= 'RESTORE LOG '+ @DBName +' FROM  DISK = N'''+ ''+@BackupPath+@BackupFileName+''' WITH  FILE = 2,NORECOVERY'

Exec (@SQL)
GO
