# sql-db-mirror #


Automatic Database Mirroring on SQL Server 2017

In my environment, the Primary Database Server – AD1SHUPCSQL22.ADAK.COM & the Secondary Database Server – AD1SHUPCSQL19.ADAK.COM
Primary SQL Instance - AD1SHUPCSQL22\SRV2
Secondary SQL Instance - AD1SHUPCSQL19\SRV2

Please add your SQL Server Service Account [under which the SQL Services – SQL Server & SQL Server Agent are running in the corresponding database servers] in Administrator group and also in both the SQL Instance as a SYSADMIN role. As this is fully automated, the Data/Log Files Path of the databases should be same for both the servers.


Below are the one-time activities which needs to be performed manually [ Folder Name: Initial execution using SSMS – Manually]
	•	Open SQL Server Management Studio [SSMS] and connect to the Primary Database Instance and execute the below script. It will create a Linked Server.

Script Name: LinkServerOnPrincipalInstance.sql

	•	Open SQL Server Management Studio [SSMS] and connect to the Secondary Database Instance and execute the below script. It will create a Linked Server.

Script Name: LinkServerOnMirroredInstance.sql

	•	Execute the below file for creation of Shared Location where backup files will be stored in the Primary Database Server. Please modify the path [C:\Backups] as per your requirement.

Script Name: CreateFileShare.cmd

	•	Execute the below script on Primary Database Server. This script will create a table which will be used as a tracker of database creation and automation of database mirroring.
Script Name: CreateAddDBMirroringTable.sql

	•	Execute the below script which will capture the event of the database creation activity. This is a DDL Trigger which needs to be executed on Primary Database Server.

Script Name: ddl_trig_database.sql

	•	Execute the below script which will create the SQL Server Job. This job will execute Master key, Certificate, Backup, Restore, Endpoint, Partner creation steps. Execution needs to be done on Primary Database Server.

Script Name: Database_Mirroring_Automation.sql

	•	Execute the below script which will create a Trigger on AddDBMirroring table. Execution needs to be done on Primary Database Server.

Script Name: Trg_InvokeSQLMirroringSetupJob.sql


Now, let’s discuss through two scenarios.
SCENARIO 1
If you create any new blank database on the production server then the mirrored database will be created automatically.

SCENARIO 2
If you restore a database as a new database name on the Primary database server, then there will be manual effort. You must issue a statement like below. You must issue insert statement for each new database. Because there is no DDL Trigger for restoration to capture event and DML Trigger isn’t applicable to the System Tables. This is applicable to existing databases on Primary database server as well.
INSERT INTO AddDBMirroring VALUES (‘NEWDB’, GETDATE(),NULL,NULL)

How it works
If you create a new blank database, one record will be inserted automatically into the table AddDBMirroring. After that, one after trigger will be fired which will initiate the SQL Server Job Database_Mirroring_Automation. This job will create Master Key, Certificate, Endpoints, Logins, Backup, Restoration and partner.

For other cases, please read the above scenarios.

I am uploading all the scripts into the chat window. Please change the Primary & Mirror server details accordingly in the scripts.


# How to contribute:

* Fork the repository
* Commit changes to your own repo
* Submit a pull request back to this repo when it is ready for review