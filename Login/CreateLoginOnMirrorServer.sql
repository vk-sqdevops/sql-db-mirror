USE master
go

IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE name like 'HOST_A_login')
BEGIN
 
	CREATE LOGIN HOST_A_login WITH PASSWORD = 'Abcd@1234';  

END
ELSE

BEGIN
 PRINT 'Login already exists'
END 

USE master
go
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE name like 'HOST_A_user')
BEGIN
 
	CREATE USER HOST_A_user FOR LOGIN HOST_A_login;  

END
ELSE

BEGIN
 PRINT 'User already exists'
END 


Use master
go

CREATE CERTIFICATE HOST_A_cert AUTHORIZATION HOST_A_user FROM FILE = '\\AD1SHUPCSQL22\BACKUPS\HOST_A_cert.cer'  
GO

GRANT CONNECT ON ENDPOINT::Endpoint_Mirroring TO [HOST_A_login];  
GO 