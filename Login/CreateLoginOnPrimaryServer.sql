USE master
go

IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE name like 'HOST_B_login')
BEGIN
 
	CREATE LOGIN HOST_B_login WITH PASSWORD = 'Abcd@1234';  

END
ELSE

BEGIN
 PRINT 'Login already exists'
END 

USE master
go
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE name like 'HOST_B_user')
BEGIN
 
	CREATE USER HOST_B_user FOR LOGIN HOST_B_login;  

END
ELSE

BEGIN
 PRINT 'User already exists'
END 

USE master
go

CREATE CERTIFICATE HOST_B_cert AUTHORIZATION HOST_B_user FROM FILE = '\\AD1SHUPCSQL22\BACKUPS\HOST_B_cert.cer'  
GO


GRANT CONNECT ON ENDPOINT::Endpoint_Mirroring TO [HOST_B_login];  
GO 
